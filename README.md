[![pipeline status](https://gitlab.inria.fr/git-tutorial/dummymaths/badges/main/pipeline.svg)](https://gitlab.inria.fr/git-tutorial/dummymaths/-/commits/main) 

# Dummy maths

Python project implementing very simple mathematical functions.

Available functions are `add`, `sub`, `multiply`, `divide` and `power`

## Testing

Unit tests and style checks can be run using [tox](https://tox.wiki/en/latest/).
Install `tox` using `pip`:

```
pip install tox
```

Then run the tests and style checks with:

```
tox
```

## License

This project is published under the 3-Clause BSD license.
